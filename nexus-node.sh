#!/bin/sh

# save the artifact details in a json file
curl -u {user}:{password} -X GET '{server_ip}:{nexus_port}/service/rest/v1/components?repository=npm-snapshots&sort=version' | jq "." > artifact.json

# grab the download url from the saved artifact details using 'jq' json processor tool
artifactDownloadUrl=$(jq '.items[].assets[].downloadUrl' artifact.json --raw-output)

# fetch the artifact with the extracted download url using 'wget' tool
wget --http-user={user} --http-password={password} $artifactDownloadUrl

tarfile=$(basename "$artifactDownloadUrl")
tar -xvzf "$tarfile"
cd package

(npm install && npm start) &
